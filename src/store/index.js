import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { addCategoryToRepo } from './helper'

Vue.use(Vuex)

const state = {
  repos: [],
  categories: [
    'All',
    'Public',
    'Private',
    'Sources',
    'Forks'
  ],
  categoriesCount: [],
  areReposLoading: false,
  areReposLoaded: false
}

const getters = {
  getRepos: state => ({category, param, loadAll}) => {
    let filteredRepos = state.repos
      .filter(
        repo => repo.categories.includes(category) && repo.name.includes(param)
      )
    if (loadAll) return filteredRepos
    else return filteredRepos.slice(0, 3)
  },
  getReposLoading: state => state.areReposLoading,
  getReposLoaded: state => state.areReposLoaded,
  getCategories: state => state.categories,
  getCategoryCount: state => category => {
    let selectedCategory = state.categoriesCount.find(c => c.category === category)
    if (selectedCategory) return selectedCategory.count
    return 0
  }
}

export const mutations = {
  addRepos (state, repos) {
    state.repos = repos
  },
  toggleLoading (state) {
    state.areReposLoading = !state.areReposLoading
  },
  toggleLoaded (state) {
    state.areReposLoaded = !state.areReposLoaded
  },
  addCategoryCount (state, categoriesCount) {
    state.categoriesCount = categoriesCount
  }
}

export const actions = {
  loadRepos ({commit}) {
    commit('toggleLoading')
    return axios.get('https://api.github.com/users/goschevski/repos')
      .then(
        repsonse => {
          let data = addCategoryToRepo(repsonse.data)
          commit('addRepos', data.repos)
          commit('addCategoryCount', data.categoriesCount)
          commit('toggleLoading')
          commit('toggleLoaded')
        },
        error => {
          console.log(error)
          commit('toggleLoading')
        }
      )
  }
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})
