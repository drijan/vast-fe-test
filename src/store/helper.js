export const addCategoryToRepo = repos => {
  let allCount = 0
  let publicCount = 0
  let privateCount = 0
  let sourcesCount = 0
  let forksCount = 0
  repos.map(repo => {
    repo.categories = []
    repo.categories.push('All')
    allCount++
    if (repo.fork) {
      repo.categories.push('Forks')
      forksCount++
    } else {
      repo.categories.push('Sources')
      sourcesCount++
    }
    if (repo.private) {
      repo.categories.push('Private')
      privateCount++
    } else {
      repo.categories.push('Public')
      publicCount++
    }
    return repo
  })
  let categoriesCount = [{
    category: 'All',
    count: allCount
  }, {
    category: 'Public',
    count: publicCount
  }, {
    category: 'Private',
    count: privateCount
  }, {
    category: 'Sources',
    count: sourcesCount
  }, {
    category: 'Forks',
    count: forksCount
  }]
  return {repos, categoriesCount}
}
