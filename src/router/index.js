import Vue from 'vue'
import Router from 'vue-router'
import ReposMain from '@/components/Main'
import Documentation from '@/components/Documentation'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'repos',
      component: ReposMain
    },
    {
      path: '/docs',
      name: 'docs',
      component: Documentation
    }
  ]
})
