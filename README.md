# vast-fe-test

> A Vue.js project for JS interview proccess in Vast

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

## Tested with
* NodeJS v8.10.0
* npm 5.8.0
* VueJS 2.5.16
* Vuex 3.0.1
* Chrome 65
* Firefox 59

## Folder structure

```
- /config         - Configuration files
- /src            - Vue.js app source
  - /components   - All Vue components
    - /icons      - source and fork icons
    - /repos      - Main module components (list of repos, with search, category filter, ...)
  - /store        - Vuex store 
- /test           - Contains unit test files
  - /unit         -
```

## Documentation

Documentation is made with propdoc, and it is available at address http://localhost:8080/docs.
