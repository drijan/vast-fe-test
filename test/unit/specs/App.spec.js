import Vuex from 'vuex'
import { shallow, createLocalVue } from '@vue/test-utils'
import App from '@/App'

const localVue = createLocalVue()
localVue.use(Vuex)

let actions = {
  loadRepos: jest.fn()
}
let store = new Vuex.Store({
  actions
})

const createComponent = () => shallow(App, {store, localVue})

describe('App component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent()
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a App component', () => {
    expect(cmp.is(App)).toBe(true)
  })

  it('has created hook', () => {
    expect(App.created).toBeDefined()
    expect(typeof App.created).toBe('function')
  })

  describe('App methods', () => {
    it('has methods object', () => {
      expect(App.methods).toBeTruthy()
      expect(typeof App.methods).toBe('object')
    })

    it('has loadRepos method', () => {
      expect(cmp.vm.loadRepos).toBeTruthy()
      expect(typeof cmp.vm.loadRepos).toBe('function')
    })
  })
})
