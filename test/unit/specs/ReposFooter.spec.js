import { mount } from '@vue/test-utils'
import ReposFooter from '@/components/repos/ReposFooter'

const createComponent = () => mount(ReposFooter)

describe('ReposFooter component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent()
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a ReposHeader component', () => {
    expect(cmp.is(ReposFooter)).toBe(true)
  })

  describe('ReposFooter data', () => {
    it('has data', () => {
      expect(ReposFooter.data).toBeTruthy()
    })

    it('data is of type function', () => {
      expect(typeof ReposFooter.data).toBe('function')
    })

    it('moreShow in data is defined and of type boolean', () => {
      expect(cmp.vm.moreShown).toBeDefined()
      expect(typeof cmp.vm.moreShown).toBe('boolean')
    })
  })

  describe('ReposFooter methods', () => {
    it('has methods object', () => {
      expect(ReposFooter.methods).toBeTruthy()
      expect(typeof ReposFooter.methods).toBe('object')
    })

    it('has loadMore method', () => {
      expect(cmp.vm.loadMore).toBeTruthy()
      expect(typeof cmp.vm.loadMore).toBe('function')
    })

    it('has loadLess method', () => {
      expect(cmp.vm.loadLess).toBeTruthy()
      expect(typeof cmp.vm.loadLess).toBe('function')
    })

    it('on loadMore and loadLess methods execute, moreShown in data is toggled from true to false and back', () => {
      expect(cmp.vm.moreShown).toBe(false)
      cmp.vm.loadMore()
      expect(cmp.vm.moreShown).toBe(true)
      cmp.vm.loadLess()
      expect(cmp.vm.moreShown).toBe(false)
    })

    describe('events are emitted on methods calls', () => {
      it('loadMore event is emitted on loadMore() method execute, without payload', () => {
        cmp.vm.loadMore()
        expect(cmp.emitted().loadMore).toBeTruthy()
        expect(cmp.emitted().loadMore.length).toBe(1)
        expect(cmp.emitted().loadMore[0].length).toBe(0)
      })

      it('loadLess event is emitted on loadLess() method execute, without payload', () => {
        cmp.vm.loadLess()
        expect(cmp.emitted().loadLess).toBeTruthy()
        expect(cmp.emitted().loadLess.length).toBe(1)
        expect(cmp.emitted().loadLess[0].length).toBe(0)
      })
    })
  })
})
