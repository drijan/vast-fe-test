import { mount } from '@vue/test-utils'
import ReposBody from '@/components/repos/ReposBody'
import ForkIcon from '@/components/icons/Fork'
import SourceIcon from '@/components/icons/Source'
import RepoName from '@/components/repos/RepoName'

const createComponent = propsData => mount(ReposBody, {propsData})

const propsData = {
  repos: [{
    'id': 77372176,
    'name': 'adonis-framework',
    'full_name': 'goschevski/adonis-framework',
    'private': false,
    'html_url': 'https://github.com/goschevski/adonis-framework',
    'description': 'NodeJs Web Application Framework. Makes it easy for you to write webapps with less code :smiley:',
    'fork': true
  }],
  charsToEmphasize: 'framework'
}

describe('ReposBody component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent(propsData)
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a ReposHeader component', () => {
    expect(cmp.is(ReposBody)).toBe(true)
  })

  it('contains ForkIcon component', () => {
    expect(cmp.contains(ForkIcon)).toBe(true)
  })

  it('contains SourceIcon components', () => {
    expect(cmp.contains(SourceIcon)).toBe(true)
  })

  it('contains RepoName components', () => {
    expect(cmp.contains(RepoName)).toBe(true)
  })

  describe('ReposBody properties', () => {
    it('has repos property', () => {
      expect(cmp.props('repos')).toBeTruthy()
    })

    it('has charsToEmphasize property', () => {
      expect(cmp.props('charsToEmphasize')).toBeTruthy()
    })

    it('repos is instance of Array', () => {
      expect(cmp.vm.repos instanceof Array).toBe(true)
    })

    it('charsToEmphasize is of type String', () => {
      expect(typeof cmp.vm.charsToEmphasize).toBe('string')
    })

    it('repos array has one item', () => {
      expect(cmp.vm.repos.length).toBe(1)
    })
  })
})
