import { mount } from '@vue/test-utils'
import RepoName from '@/components/repos/RepoName'

const createComponent = propsData => mount(RepoName, {propsData})

const propsData = {
  repoName: 'some repo',
  charsToEmphasize: 'some'
}

describe('RepoName component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent(propsData)
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a Repos component', () => {
    expect(cmp.is(RepoName)).toBe(true)
  })

  describe('RepoName properties', () => {
    it('has repoName property', () => {
      expect(cmp.props('repoName')).toBeTruthy()
    })

    it('has charsToEmphasize property', () => {
      expect(cmp.props('charsToEmphasize')).toBeTruthy()
    })

    it('repoName is of type String', () => {
      expect(typeof cmp.vm.repoName).toBe('string')
    })

    it('charsToEmphasize is of type String', () => {
      expect(typeof cmp.vm.charsToEmphasize).toBe('string')
    })

    it('value of repoName is "some repo"', () => {
      expect(cmp.vm.repoName).toBe('some repo')
    })

    it('value of charsToEmphasize is "some"', () => {
      expect(cmp.vm.charsToEmphasize).toBe('some')
    })
  })

  describe('RepoName computed', () => {
    it('has computed', () => {
      expect(RepoName.computed).toBeDefined()
      expect(typeof RepoName.computed).toBe('object')
    })

    it('emphasizedRepoName is defined and of type string', () => {
      expect(cmp.vm.emphasizedRepoName).toBeDefined()
      expect(typeof cmp.vm.emphasizedRepoName).toBe('string')
    })
  })
})
