import { mount } from '@vue/test-utils'
import Repos from '@/components/repos/Repos'
import ReposHeader from '@/components/repos/ReposHeader'
import ReposBody from '@/components/repos/ReposBody'
import ReposFooter from '@/components/repos/ReposFooter'

const createComponent = propsData => mount(Repos, {propsData})

const propsData = {
  repos: [],
  count: 2,
  categories: []
}

describe('Repos component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent(propsData)
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a Repos component', () => {
    expect(cmp.is(Repos)).toBe(true)
  })

  it('contains ReposHeader component', () => {
    expect(cmp.contains(ReposHeader)).toBe(true)
  })

  it('contains ReposBody component', () => {
    expect(cmp.contains(ReposBody)).toBe(true)
  })

  it('contains ReposFooter component', () => {
    expect(cmp.contains(ReposFooter)).toBe(true)
  })

  describe('Repos properties', () => {
    it('has repos property', () => {
      expect(cmp.props('repos')).toBeTruthy()
    })

    it('has count property', () => {
      expect(cmp.props('count')).toBeTruthy()
    })

    it('has categories property', () => {
      expect(cmp.props('categories')).toBeTruthy()
    })

    it('repos property is of type Array', () => {
      expect(cmp.vm.repos instanceof Array).toBe(true)
    })

    it('count property is of type number', () => {
      expect(typeof cmp.vm.count).toBe('number')
    })

    it('categories propery is of type Array', () => {
      expect(cmp.vm.categories instanceof Array).toBe(true)
    })
  })

  describe('Repos methods', () => {
    it('has methods object', () => {
      expect(Repos.methods).toBeTruthy()
      expect(typeof Repos.methods).toBe('object')
    })

    it('has updateCategory method', () => {
      expect(cmp.vm.updateCategory).toBeTruthy()
      expect(typeof cmp.vm.updateCategory).toBe('function')
    })

    it('has updateParam method', () => {
      expect(cmp.vm.updateParam).toBeTruthy()
      expect(typeof cmp.vm.updateParam).toBe('function')
    })

    it('has loadMore method', () => {
      expect(cmp.vm.loadMore).toBeTruthy()
      expect(typeof cmp.vm.loadMore).toBe('function')
    })

    it('has loadLess method', () => {
      expect(cmp.vm.loadLess).toBeTruthy()
      expect(typeof cmp.vm.loadLess).toBe('function')
    })

    describe('events are emitted on methods calls', () => {
      it('categoryUpdate event is emitted on updateCategory() method execute, with correct payload', () => {
        cmp.vm.updateCategory('All')
        expect(cmp.emitted().categoryUpdate).toBeTruthy()
        expect(cmp.emitted().categoryUpdate.length).toBe(1)
        expect(cmp.emitted().categoryUpdate[0][0]).toBe('All')
      })

      it('paramUpdate event is emitted on updateParam() method execute, with correct payload', () => {
        cmp.vm.updateParam('unit')
        expect(cmp.emitted().paramUpdate).toBeTruthy()
        expect(cmp.emitted().paramUpdate.length).toBe(1)
        expect(cmp.emitted().paramUpdate[0][0]).toBe('unit')
      })

      it('loadMore event is emitted on loadMore() method execute, without payload', () => {
        cmp.vm.loadMore()
        expect(cmp.emitted().loadMore).toBeTruthy()
        expect(cmp.emitted().loadMore.length).toBe(1)
        expect(cmp.emitted().loadMore[0].length).toBe(0)
      })

      it('loadLess event is emitted on loadLess() method without payload', () => {
        cmp.vm.loadLess()
        expect(cmp.emitted().loadLess).toBeTruthy()
        expect(cmp.emitted().loadLess.length).toBe(1)
        expect(cmp.emitted().loadLess[0].length).toBe(0)
      })
    })
  })
})
