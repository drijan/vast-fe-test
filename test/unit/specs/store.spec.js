import { mutations } from '@/store'

describe('mutations', () => {
  it('addRepos', () => {
    const state = { repos: [] }
    let repos = [{ name: 'test' }, { name: 'test2' }]
    mutations.addRepos(state, repos)
    expect(state.repos.length).toBe(2)
  })

  it('toggleLoading', () => {
    const state = { areReposLoading: false }
    mutations.toggleLoading(state)
    expect(state.areReposLoading).toBe(true)
    mutations.toggleLoading(state)
    expect(state.areReposLoading).toBe(false)
  })

  it('toggleLoaded', () => {
    const state = { areReposLoaded: false }
    mutations.toggleLoaded(state)
    expect(state.areReposLoaded).toBe(true)
    mutations.toggleLoaded(state)
    expect(state.areReposLoaded).toBe(false)
  })

  it('addCategoryCount', () => {
    const state = { categoriesCount: [] }
    let counts = [1, 2, 3]
    mutations.addCategoryCount(state, counts)
    expect(state.categoriesCount.length).toBe(3)
  })
})
