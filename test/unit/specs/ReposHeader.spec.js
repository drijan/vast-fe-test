import { mount } from '@vue/test-utils'
import ReposHeader from '@/components/repos/ReposHeader'

const createComponent = propsData => mount(ReposHeader, {propsData})

const propsData = {
  count: 10,
  categories: []
}

describe('ReposHeader component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent(propsData)
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a ReposHeader component', () => {
    expect(cmp.is(ReposHeader)).toBe(true)
  })

  describe('ReposHeader properties', () => {
    it('has count property', () => {
      expect(cmp.props('count')).toBeTruthy()
    })

    it('has categories property', () => {
      expect(cmp.props('categories')).toBeTruthy()
    })

    it('count property is of type number', () => {
      expect(typeof cmp.vm.count).toBe('number')
    })

    it('categories property is of type Array', () => {
      expect(cmp.vm.categories instanceof Array).toBe(true)
    })
  })

  describe('ReposHeader methods', () => {
    it('has methods object', () => {
      expect(ReposHeader.methods).toBeTruthy()
      expect(typeof ReposHeader.methods).toBe('object')
    })

    it('has updateCategory method', () => {
      expect(cmp.vm.updateCategory).toBeTruthy()
      expect(typeof cmp.vm.updateCategory).toBe('function')
    })

    it('has updateParam method', () => {
      expect(cmp.vm.updateParam).toBeTruthy()
      expect(typeof cmp.vm.updateParam).toBe('function')
    })

    describe('events are emitted on methods calls', () => {
      it('categoryUpdate event is emitted on updateCategory() method execute, with correct payload', () => {
        cmp.vm.updateCategory('All')
        expect(cmp.emitted().categoryUpdate).toBeTruthy()
        expect(cmp.emitted().categoryUpdate.length).toBe(1)
        expect(cmp.emitted().categoryUpdate[0][0]).toBe('All')
      })

      it('paramUpdate event is emitted on updateParam() method execute, with correct payload', () => {
        cmp.vm.updateParam()
        expect(cmp.emitted().paramUpdate).toBeTruthy()
        expect(cmp.emitted().paramUpdate.length).toBe(1)
      })
    })
  })

  describe('ReposHeader data', () => {
    it('has data', () => {
      expect(ReposHeader.data).toBeTruthy()
    })

    it('data is of type function', () => {
      expect(typeof ReposHeader.data).toBe('function')
    })

    it('searchParam in data is defined and of type string', () => {
      expect(cmp.vm.searchParam).toBeDefined()
      expect(typeof cmp.vm.searchParam).toBe('string')
    })

    it('activeCategory in data is defined and of type string', () => {
      expect(cmp.vm.activeCategory).toBeDefined()
      expect(typeof cmp.vm.activeCategory).toBe('string')
    })
  })
})
