import Vuex from 'vuex'
import { shallow, createLocalVue } from '@vue/test-utils'
import Main from '@/components/Main'

const localVue = createLocalVue()
localVue.use(Vuex)

let getters = {
  getRepos: jest.fn(),
  getReposLoading: jest.fn(),
  getReposLoaded: jest.fn(),
  getCategories: jest.fn(),
  getCategoryCount: jest.fn()
}
let store = new Vuex.Store({
  getters
})

const createComponent = () => shallow(Main, {store, localVue})

describe('Main component', () => {
  let cmp

  beforeEach(() => {
    cmp = createComponent()
  })

  it('is a Vue instance', () => {
    expect(cmp.isVueInstance()).toBe(true)
  })

  it('is a Main component', () => {
    expect(cmp.is(Main)).toBe(true)
  })

  describe('Main data', () => {
    it('has data', () => {
      expect(Main.data).toBeTruthy()
    })

    it('data is of type function', () => {
      expect(typeof Main.data).toBe('function')
    })

    it('category is defined and of type string', () => {
      expect(cmp.vm.category).toBeDefined()
      expect(typeof cmp.vm.category).toBe('string')
    })

    it('param is defined and of type string', () => {
      expect(cmp.vm.param).toBeDefined()
      expect(typeof cmp.vm.param).toBe('string')
    })

    it('loadAll is defined and of type boolean', () => {
      expect(cmp.vm.loadAll).toBeDefined()
      expect(typeof cmp.vm.loadAll).toBe('boolean')
    })

    describe('default data are set correctly', () => {
      it('category should be set to "All"', () => {
        expect(cmp.vm.category).toBe('All')
      })

      it('param should be set to empty string', () => {
        expect(cmp.vm.param).toBe('')
      })

      it('loadAll should be set to false', () => {
        expect(cmp.vm.loadAll).toBe(false)
      })
    })
  })

  describe('Main methods', () => {
    it('has methods object', () => {
      expect(Main.methods).toBeTruthy()
      expect(typeof Main.methods).toBe('object')
    })

    it('has updateCategory method', () => {
      expect(cmp.vm.updateCategory).toBeTruthy()
      expect(typeof cmp.vm.updateCategory).toBe('function')
    })

    it('has updateParam method', () => {
      expect(cmp.vm.updateParam).toBeTruthy()
      expect(typeof cmp.vm.updateParam).toBe('function')
    })

    it('has toggleLoadRepos method', () => {
      expect(cmp.vm.toggleLoadRepos).toBeTruthy()
      expect(typeof cmp.vm.toggleLoadRepos).toBe('function')
    })

    it('on updateCategory(), category in data is changed correctly', () => {
      expect(cmp.vm.category).toBe('All')
      cmp.vm.updateCategory('test')
      expect(cmp.vm.category).toBe('test')
    })

    it('on updateParam(), param in data is changed correctly', () => {
      expect(cmp.vm.param).toBe('')
      cmp.vm.updateParam('new param')
      expect(cmp.vm.param).toBe('new param')
    })

    it('on toggleLoadRepos(), loadAll in data is changed correctly', () => {
      expect(cmp.vm.loadAll).toBe(false)
      cmp.vm.toggleLoadRepos()
      expect(cmp.vm.loadAll).toBe(true)
    })

    it('after two toggleLoadRepos() calls, loadAll is false', () => {
      expect(cmp.vm.loadAll).toBe(false)
      cmp.vm.toggleLoadRepos()
      cmp.vm.toggleLoadRepos()
      expect(cmp.vm.loadAll).toBe(false)
    })
  })
})
